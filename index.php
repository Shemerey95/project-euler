<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Проект эйлера</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/script.js" defer></script>
</head>
<body>
<div class="task">
    <h1 class="task__title"></h1>
    <p class="task__text"></p>
    <p class="task__answer">Ответ: <span></span></p>
    <p class="task__time">Затраченное время на выполнения скрипта:  <span></span></p>
</div>
</body>
</html>