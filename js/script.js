// document.querySelector('.task__title').innerText = 'Задача 1:';
// document.querySelector('.task__text').innerText  = 'Найдите сумму всех чисел меньше 1000, кратных 3 или 5.';
//
// function sumOneThousand(n) {
//     let loadTime = performance.now(),
//         sum      = 0;
//
//     for (let i = 0; i <= n; i++) {
//         if ((i % 3 === 0) || (i % 5 === 0)) {
//             sum += i;
//         }
//     }
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = sum;
// }
//
// n = 1000;
// sumOneThousand(n);


// document.querySelector('.task__title').innerText = 'Задача 2:';
// document.querySelector('.task__text').innerText  = 'Каждый следующий элемент ряда Фибоначчи получается при сложении двух предыдущих. Начиная с 1 и 2, первые 10 элементов будут: 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...Найдите сумму всех четных элементов ряда Фибоначчи, которые не превышают четыре миллиона.';
//
// function numberFebonachi(n) {
//     let loadTime     = performance.now(),
//         sumOddNumber = 0,
//         mas          = [0, 1];
//
//     for (let i = 2; mas[i - 1] <= n; i++) {
//         let number = mas[i - 1] + mas[i - 2];
//         if (number <= n) {
//             mas[i] = number;
//             if (mas[i] % 2 === 0) {
//                 sumOddNumber += mas[i];
//             }
//         }
//     }
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = sumOddNumber;
// }
//
// n = 4000000;
// numberFebonachi(n);


// document.querySelector('.task__title').innerText = 'Задача 3:';
// document.querySelector('.task__text').innerHTML  = 'Простые делители числа 13195 - это 5, 7, 13 и 29. <br> Каков самый большой делитель числа 600851475143, являющийся простым числом?';
//
// function biggestDivider(number) {
//     let loadTime   = performance.now(),
//         masDivider = [],
//         divider    = 2;
//
//     for (let i = 0; number > 1; i++) {
//         let isValid = true;
//         do {
//             if (number % divider === 0) {
//                 masDivider[i] = divider;
//                 number        = number / divider;
//                 isValid       = false;
//             } else {
//                 divider++;
//             }
//         } while (isValid);
//
//     }
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = masDivider[masDivider.length - 1];
// }
//
// number = 600851475143;
// biggestDivider(number);


// document.querySelector('.task__title').innerText = 'Задача 4:';
// document.querySelector('.task__text').innerHTML  = "Число-палиндром с обеих сторон (справа налево и слева направо) читается одинаково. Самое большое число-палиндром, полученное умножением двух двузначных чисел – 9009 = 91 × 99.<br> Найдите самый большой палиндром, полученный умножением двух трехзначных чисел.";
//
// function findPalindrom(numberDischarges) {
//     let loadTime  = performance.now(),
//         palindrom = 0,
//         firstNumber,
//         secondNumber;
//
//     for (let i = 0; i < Math.pow(10, numberDischarges); i++) {
//         for (let j = 0; j < Math.pow(10, numberDischarges); j++) {
//             let number        = i * j,
//                 stringNumber  = number.toString(),
//                 firstHalf     = stringNumber.substring(0, numberDischarges),
//                 secondHalf    = stringNumber.substring(numberDischarges, 2 * numberDischarges),
//                 reverseSecond = secondHalf.split('').reverse().join('');
//             if (firstHalf === reverseSecond) {
//                 palindrom    = number;
//                 firstNumber  = i;
//                 secondNumber = j;
//             }
//         }
//     }
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = palindrom + '; Числа: ' + firstNumber + ', ' + secondNumber;
// }
//
// numberDischarges = 3;
// findPalindrom(numberDischarges);


// document.querySelector('.task__title').innerText = 'Задача 5:';
// document.querySelector('.task__text').innerHTML  = "2520 - самое маленькое число, которое делится без остатка на все числа от 1 до 10.<br>Какое самое маленькое число делится нацело на все числа от 1 до 20?";
//
// function biggestDividend(countDivider) {
//
//     // 1 способ решения
// //     let loadTime = performance.now(),
// //         number   = 1;
// //
// //     do {
// //         var flag      = false,
// //             flagLocal = true;
// //         for (let divider = 1; divider <= countDivider && flagLocal; divider++) {
// //             if (number % divider !== 0) {
// //                 flag      = true;
// //                 flagLocal = false;
// //             }
// //         }
// //
// //         number++;
// //
// //         if (flag === false) {
// //             number--;
// //         }
// //     } while (flag);
// //
// //     loadTime = (performance.now() - loadTime) / 1000;
// //     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
// //     document.querySelector('.task__answer span').innerText = number;
// // }
//
// // 2 способ решения
//     let loadTime         = performance.now(),
//         number           = 1,
//         masNumber        = [2],
//         productNumbers   = masNumber[0],
//         lastSimpleNumber = masNumber[masNumber.length - 1];
//
//     for (var i = 1; lastSimpleNumber <= countDivider; i++) {
//         let flag = true;
//         lastSimpleNumber++;
//
//         do {
//             var flag2 = true;
//
//             for (var j = 0; j < masNumber.length && flag2; j++) {
//                 if (lastSimpleNumber / masNumber[j] > 1 && lastSimpleNumber % masNumber[j] === 0) {
//                     flag2 = false;
//                 }
//             }
//
//             if (flag2) {
//                 masNumber[i] = lastSimpleNumber;
//                 flag         = false;
//                 productNumbers *= lastSimpleNumber;
//             } else {
//                 lastSimpleNumber++;
//             }
//         } while (flag);
//
//         if (lastSimpleNumber >= countDivider) {
//             masNumber.splice(masNumber.length - 1, 1);
//             productNumbers /= lastSimpleNumber;
//         }
//     }
//
//     do {
//         var flag      = false,
//             flagLocal = true;
//         for (let divider = 1; divider <= countDivider && flagLocal; divider++) {
//             if (number % divider !== 0) {
//                 flag      = true;
//                 flagLocal = false;
//             }
//         }
//
//         if (flag === false) {
//             number -= productNumbers;
//         }
//
//         if (number === 1) {
//             number = productNumbers;
//         }
//
//         number += productNumbers;
//
//     } while (flag);
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = number;
// }
//
// countDivider = 20;
// biggestDividend(countDivider);


// document.querySelector('.task__title').innerText = 'Задача 6:';
// document.querySelector('.task__text').innerHTML  = "Сумма квадратов первых десяти натуральных чисел равна<br>1^2 + 2^2 + ... + 10^2 = 385<br><br>Квадрат суммы первых десяти натуральных чисел равен<br>(1 + 2 + ... + 10)^2 = 55^2 = 3025<br><br>Следовательно, разность между суммой квадратов и квадратом суммы первых десяти натуральных чисел составляет<br>3025 − 385 = 2640.<br><br>Найдите разность между суммой квадратов и квадратом суммы первых ста натуральных чисел.";
//
// function getDifference(number) {
//     let loadTime        = performance.now() / 1000,
//         sumNumber       = 0,
//         squereSumNumber = 0,
//         squereSum       = 0,
//         difference      = 0;
//
//     for (var i = 1; i <= number; i++) {
//         sumNumber += i;
//         squereSumNumber += Math.pow(i, 2);
//     }
//     squereSum  = Math.pow(sumNumber, 2);
//     difference = squereSum - squereSumNumber;
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = difference;
// }
//
// number = 20;
// getDifference(number);


// document.querySelector('.task__title').innerText = 'Задача 7:';
// document.querySelector('.task__text').innerHTML  = "Выписав первые шесть простых чисел, получим 2, 3, 5, 7, 11 и 13. Очевидно, что 6-ое простое число - 13.<br><br>Какое число является 10001-ым простым числом?";
//
// function getSimpleNumber(number) {
//     let loadTime  = performance.now(),
//         masNumber = [];
//     masNumber[1]  = [2];
//
//     for (var i = 2; i <= number; i++) {
//         let lastSimpleNumber = masNumber[masNumber.length - 1],
//             flag             = true;
//
//         lastSimpleNumber++;
//         do {
//             var flag2 = true;
//
//             for (var j = 1; j <= Math.sqrt(lastSimpleNumber) + 2 && flag2; j++) {
//                 if (lastSimpleNumber / masNumber[j] > 1 && lastSimpleNumber % masNumber[j] === 0) {
//                     flag2 = false;
//                 }
//             }
//
//             if (flag2) {
//                 masNumber[i] = lastSimpleNumber;
//                 flag         = false;
//             } else {
//                 lastSimpleNumber++;
//             }
//         } while (flag);
//     }
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = masNumber[number];
// }
//
// number = 10001;
// getSimpleNumber(number);


// document.querySelector('.task__title').innerText = 'Задача 8:';
// document.querySelector('.task__text').innerHTML  = "Наибольшее произведение четырех последовательных цифр в нижеприведенном 1000-значном числе равно 9 × 9 × 8 × 9 = 5832.<br>Число: <br><br>7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450<br><br>Найдите наибольшее произведение тринадцати последовательных цифр в данном числе.";
//
// function getProductNumbers(lengthNumbers) {
//     let loadTime        = performance.now() / 1000,
//         stringNumber    = "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450",
//         productNumbers  = 0,
//         mainMassNumbers = [];
//
//     for (let i = 0; i <= (stringNumber.length - lengthNumbers); i++) {
//         let localProductNumbers     = 1,
//             localProductNumbersMass = [];
//         for (let j = 0; j < lengthNumbers; j++) {
//             localProductNumbers        = stringNumber[i + j] * localProductNumbers;
//             localProductNumbersMass[j] = stringNumber[i + j];
//         }
//         if (productNumbers < localProductNumbers) {
//             productNumbers  = localProductNumbers;
//             mainMassNumbers = localProductNumbersMass;
//         }
//     }
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = productNumbers + '; числа: ' + mainMassNumbers;
// }
//
// lengthNumbers = 30;
// getProductNumbers(lengthNumbers);


// document.querySelector('.task__title').innerText = 'Задача 9:';
// document.querySelector('.task__text').innerHTML  = "Тройка Пифагора - три натуральных числа a < b < c, для которых выполняется равенство<br><br>a^2 + b^2 = c^2<br><br>Например, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.<br><br>Существует только одна тройка Пифагора, для которой a + b + c = 1000.<br><br>Найдите произведение abc.";
//
// function getNumberPifagor(sumNumbers) {
//     let loadTime  = performance.now() / 1000,
//         numberA   = 0,
//         numberB   = 0,
//         numberC   = 0,
//         poductABC = 0;
//
//     for (let i = 1; i <= sumNumbers; i++) {
//         for (let j = 1; j <= sumNumbers; j++) {
//             let aa      = i * i,
//                 bb      = j * j,
//                 sumAaBb = aa + bb,
//                 a       = Math.sqrt(aa),
//                 b       = Math.sqrt(bb),
//                 c       = Math.sqrt(sumAaBb);
//             if (Number.isInteger(c) && (a + b + c === sumNumbers)) {
//                 numberA   = a;
//                 numberB   = b;
//                 numberC   = c;
//                 poductABC = a * b * c;
//             }
//         }
//     }
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = poductABC + '; числа: ' + numberA + ', ' + numberB + ', ' + numberC;
// }
//
// sumNumbers = 1000;
// getNumberPifagor(sumNumbers);


// document.querySelector('.task__title').innerText = 'Задача 10:';
// document.querySelector('.task__text').innerHTML  = "Сумма простых чисел меньше 10 равна 2 + 3 + 5 + 7 = 17.<br><br>Найдите сумму всех простых чисел меньше двух миллионов.";
//
// function getSunSimpleNumbers(maxSimpleNumber) {
//     let loadTime         = performance.now(),
//         masNumber        = [2],
//         sumNumbers       = masNumber[0],
//         lastSimpleNumber = masNumber[masNumber.length - 1];
//
//     for (var i = 1; lastSimpleNumber <= maxSimpleNumber; i++) {
//         let flag = true;
//         lastSimpleNumber++;
//
//         do {
//             var flag2 = true;
//
//             for (var j = 0; j < Math.sqrt(lastSimpleNumber) + 2 && flag2; j++) {
//                 if (lastSimpleNumber / masNumber[j] > 1 && lastSimpleNumber % masNumber[j] === 0) {
//                     flag2 = false;
//                 }
//             }
//
//             if (flag2) {
//                 masNumber[i] = lastSimpleNumber;
//                 sumNumbers += lastSimpleNumber;
//                 flag         = false;
//             } else {
//                 lastSimpleNumber++;
//             }
//         } while (flag);
//
//         if (lastSimpleNumber >= maxSimpleNumber) {
//             masNumber.splice(masNumber.length - 1, 1);
//             sumNumbers -= lastSimpleNumber;
//         }
//     }
//
//     loadTime = (performance.now() - loadTime) / 1000;
//     document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
//     document.querySelector('.task__answer span').innerText = sumNumbers;
// }
//
// maxSimpleNumber = 2000000;
// getSunSimpleNumbers(maxSimpleNumber);


document.querySelector('.task__title').innerText = 'Задача 12:';
document.querySelector('.task__text').innerHTML  = "Последовательность треугольных чисел образуется путем сложения натуральных чисел. К примеру, 7-ое треугольное число равно 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. Первые десять треугольных чисел:<br><br>1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...<br><br>Перечислим делители первых семи треугольных чисел:<br><br>1: 1<br>3: 1, 3<br>6: 1, 2, 3, 6<br>10: 1, 2, 5, 10<br>15: 1, 3, 5, 15<br>21: 1, 3, 7, 21<br>28: 1, 2, 4, 7, 14, 28<br><br>Как мы видим, 28 - первое треугольное число, у которого более пяти делителей.<br><br>Каково первое треугольное число, у которого более пятисот делителей?";

function getTriangularNumber(devidersNumber) {
    let loadTime = performance.now() / 1000,
        flag       = true,
        lastNumber = 0,
        prevNumber = 0,
        extremeBorder = 0;

    if (devidersNumber <= 100) {
        extremeBorder = 4;
    } else if (devidersNumber > 100 && devidersNumber <= 300) {
        extremeBorder = 64;
    } else if (devidersNumber > 300) {
        extremeBorder = 128;
    } else if (devidersNumber >= 600) {
        extremeBorder = 256;
    }

    for (var i = 1; flag; i++) {
        lastNumber = prevNumber + i;
        var countDelivors = 0;
        for (var j = 1; j <= lastNumber / extremeBorder; j++) {
            if (lastNumber % j === 0) {
                countDelivors += 1;
            }
        }

        countDelivors += extremeBorder / 2;

        prevNumber = lastNumber;

        if (countDelivors > devidersNumber) {
            flag = false;
        }
    }

    loadTime = (performance.now() - loadTime) / 1000;
    document.querySelector('.task__time span').innerText   = loadTime.toFixed(3) >= 60 ? (loadTime / 60).toFixed(3) + ' минут(ы)': loadTime.toFixed(3) + ' секунд(ы)';
    document.querySelector('.task__answer span').innerHTML = lastNumber;
}

devidersNumber = 500;
getTriangularNumber(devidersNumber);
